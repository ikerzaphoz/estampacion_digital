<?php

//Mostramos errores
ini_set("display_errors", 1);

//Constantes
define("root", $_SERVER['DOCUMENT_ROOT']."/projects/estampaciondigital/");
define("path_web", "/projects/estampaciondigital/web/");
define("path_admin", "/projects/estampaciondigital/admin/");

//Clases
require 'class/conf.php';
require 'class/db.php';

?>