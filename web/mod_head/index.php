<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Estampación Digital Flexol</title>
    <meta name="viewport" content="width=device-width,height=device-height initial-scale=1">
    <link rel="stylesheet" type="text/css" href="<?=path_web?>css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=path_web?>external_libraries/bootstrapSelect/bootstrap-select.min.css">
    <link rel="stylesheet" type="text/css" href="<?=path_web?>external_libraries/toastr-master/toastr.min.css">
    <link rel="stylesheet" type="text/css" href="<?=path_web?>css/bootstrap-colorpicker.min.css">
    <link rel="stylesheet/less" type="text/css" href="<?=path_web?>css/style.css?v=<?=microtime();?>">
</head>
<body>