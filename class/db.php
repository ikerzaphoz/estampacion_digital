<?php

Class Db{

   private $servidor;
   private $usuario;
   private $password;
   private $base_datos;
   private $link;
   private $stmt;
   private $array;

   public function __construct(){
      $this->setConexion();
      $this->conectar();
   }

   public function setConexion(){
      $conf = new Conf();
      $this->servidor=$conf->getHostDB();
      $this->base_datos=$conf->getDB();
      $this->usuario=$conf->getUserDB();
      $this->password=$conf->getPassDB();
   }

   public function conectar(){
      $this->link=mysqli_connect($this->servidor, $this->usuario, $this->password);
      if (mysqli_connect_errno()):
         die(sprintf("[%d] %s\n", mysqli_connect_errno(), mysqli_connect_error()));
      endif;
      mysqli_select_db($this->link,$this->base_datos);
      mysqli_query($this->link, "SET NAMES 'utf8'");
      if(mysqli_error($this->link)):
         echo("Error description: " . mysqli_error($this->link));
      endif;
   }

   public function ejecutar($sql){
      $this->stmt=mysqli_query($this->link,$sql);
      if(mysqli_error($this->link)):
         echo("Error description: " . mysqli_error($this->link));
      endif;
      return $this->stmt;
   }

   public function ejecutarReturnAffected($sql){
      $this->stmt=mysqli_query($this->link,$sql);
      if(mysqli_error($this->link)):
      endif;
      return mysqli_affected_rows($this->link);
   }

   public function obtener_fila($stmt,$fila){
      if ($fila==0){
         $this->array=mysqli_fetch_array($stmt);
      }else{
         mysqli_data_seek($stmt,$fila);
         $this->array=mysqli_fetch_array($stmt);
      }
      return $this->array;
   }

   public function lastID(){
      return mysqli_insert_id($this->link);
   }

   public function obtener_consultas($sql, $index = NULL){
      $this->stmt=mysqli_query($this->link,$sql);
      if(mysqli_error($this->link)):
         echo("Error description: " . mysqli_error($this->link));
      endif;
      $array = [];
      while($row = mysqli_fetch_array($this->stmt))
      {
         if(empty($index)):
               $array[] = $row;
            else:
               $array[$row[$index]] = $row;
         endif;
      }
      return $array;
   }

}

?>